public class Rectangle extends Figure{
    public String fName;
    public int fLength;
    public int fWidth;
    public Rectangle(double x, double y, String figure, int length, int width) {
        super(x, y);
        fName = figure;
        fLength = length;
        fWidth = width;
    }
    @Override
    public void square() {
        int square = fLength * fWidth;
        System.out.println(fName + "- Площадь: " + square);
    }
}
