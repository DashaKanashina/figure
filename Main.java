public abstract class Main {
    public static void main(String[] args) {
        Figure[] myFigure = {
                new Circle(-5, 5, "Круг ", (int) (Math.random() * 11)),
                new Rectangle(10, -10, "Прямоугольник ", (int) (Math.random() * 21), (int) (Math.random() * 21)),
                new Circle(4, 3, "Круг ", (int) (Math.random() * 11)),
                new Rectangle(-20, -20, "Прямоугольник ", (int) (Math.random() * 21), (int) (Math.random() * 21)),
                new Circle(-2,0, "Круг ", (int) (Math.random() * 11))};
        for (int i = 0; i <= 4; i++){
            myFigure[i].square();
            myFigure[i].getQuadrant();
        }
    }
}
