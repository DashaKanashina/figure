public class Circle extends Figure{
    public String fName;
    public int fRadius;
    public Circle(double x, double y, String figure, int radius) {
        super(x, y);
        fName = figure;
        fRadius = radius;
    }
    @Override
    public void square() {
        double square = 3.14 * fRadius * fRadius;
        System.out.println(fName + "- Площадь: " + square);
    }
}