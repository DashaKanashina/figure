public abstract class Figure {
    public double fX;
    public double fY;
    public abstract void square();
    public Figure(double x, double y){
        fX = x;
        fY = y;
    }
    public void getQuadrant() {
        if (fX > 0 && fY > 0) {
            System.out.println("Центр фигуры лежит в I четверти");
        }
        if (fX < 0 && fY > 0) {
            System.out.println("Центр фигуры лежит во II четверти");
        }
        if (fX < 0 && fY < 0) {
            System.out.println("Центр фигуры лежит в III четверти");
        }
        if (fX > 0 && fY < 0) {
            System.out.println("Центр фигуры лежит в IV четверти");
        }
        if (fX == 0 || fY == 0) {
            System.out.println("Центр фигуры лежит на координатной оси");
        }
    }
}